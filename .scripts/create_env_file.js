const fs = require("fs")
const path = require("path")

const envFile = `REACT_APP_MAP_API_KEY=
REACT_APP_PRODUCTION_API_ENDPOINT=
REACT_APP_DEVELOPMENT_API_ENDPOINT=
GENERATE_SOURCEMAP=false
PORT=3000`

fs.writeFileSync(path.join(process.cwd(), ".env"), envFile)
