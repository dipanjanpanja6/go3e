const joi = require("joi")
const path = require("path")
require("dotenv").config({ path: path.join(__dirname, "../.env") })

const envVarsSchema = joi
  .object()
  .keys({
    // NODE_ENV: joi.string().valid("production", "development", "test").required(),
    // REACT_APP_MAP_API_KEY: joi.string().required().description("Map api secret"),
    // REACT_APP_PRODUCTION_API_ENDPOINT: joi.string().uri().required().description("My api "),
    // REACT_APP_DEVELOPMENT_API_ENDPOINT: joi.string().uri().required().description("My api "),
    // GENERATE_SOURCEMAP:joi.boolean().required(),
    PORT: joi.number().positive().required(),
  })
  .unknown()

const { value: envVars, error } = envVarsSchema.prefs({ errors: { label: "key" } }).validate(process.env)

if (error) {
  throw new Error(`Config validation error: ${error.message}`)
}
